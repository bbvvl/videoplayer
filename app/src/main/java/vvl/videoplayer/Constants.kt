package vvl.videoplayer


import android.os.Environment
import java.io.File

val EXTERNAL_STORAGE: String = Environment.getExternalStorageDirectory().absolutePath
val EXTERRNAL_STORAGE_FILE: File = Environment.getExternalStorageDirectory().absoluteFile
val DEBUG: Boolean = false
val FILEPROVIDER: String = "vvl.videoplayer.fileprovider"