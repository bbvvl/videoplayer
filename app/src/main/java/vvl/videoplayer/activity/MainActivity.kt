package vvl.videoplayer.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import fr.castorflex.android.verticalviewpager.VerticalViewPager
import vvl.videoplayer.DEBUG
import vvl.videoplayer.R
import vvl.videoplayer.fragments.ContentFragmentAdapter
import vvl.videoplayer.fragments.FragmentLocalList
import vvl.videoplayer.viewpager.ZoomOutTransformer


class MainActivity : AppCompatActivity() {

    lateinit var mVerticalViewPager: VerticalViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        if (DEBUG) println("MainActivity: onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initVP()

    }

    private fun initVP() {
        mVerticalViewPager = findViewById(R.id.activity_main_vertical_view_pager)
        mVerticalViewPager.setPageTransformer(false, ZoomOutTransformer())
        mVerticalViewPager.adapter = ContentFragmentAdapter.Holder(supportFragmentManager)
                .add(FragmentLocalList.newInstance()).set()

        mVerticalViewPager.overScrollMode = View.OVER_SCROLL_NEVER

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        println("SAVEINST")
    }

    override fun onStop() {
        super.onStop()
        if (DEBUG) println("MainActivity: onStop")
    }

    override fun onResume() {
        super.onResume()
        if (DEBUG) println("MainActivity: onResume")
    }
}

