package vvl.videoplayer.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import vvl.videoplayer.R
import vvl.videoplayer.filesystem.FileManager
import vvl.videoplayer.filesystem.FileUtil
import java.io.File


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)
        async(UI) {
            FileManager.update().await()
            for (q in FileManager.getList()) {
                val pos = q.hashCode()
                FileManager.getInfoList().append(pos, FileUtil.getFileInfo(q).await())

                FileManager.getBitmapList().append(pos, FileUtil.getBitmap(this@SplashActivity, q).await())
            }
            FileManager.setBackup(FileManager.getList().clone() as ArrayList<File>)
            val i = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(i)
            finish()
        }
    }
}