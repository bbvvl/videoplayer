package vvl.videoplayer.activity

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import com.khizar1556.mkvideoplayer.MKPlayer
import vvl.videoplayer.DEBUG
import vvl.videoplayer.R
import vvl.videoplayer.filesystem.FileManager

class VideoPlayerActivity : AppCompatActivity() {
    private var player: MKPlayer? = null

    private var mPos: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        window.statusBarColor = Color.TRANSPARENT
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        window.decorView.systemUiVisibility =
                (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)

        mPos = intent.getIntExtra("url", 0)
        player = MKPlayer(this)
        player!!.setPadding(getStatusBarHeight())
        player!!.setPlayerCallbacks(object : MKPlayer.playerCallbacks {
            override fun onNextClick() {
                if (mPos != FileManager.getList().size - 1) {
                    mPos++
                } else
                    mPos = 0

                player!!.play(FileManager.getList()[mPos].absolutePath)
                player!!.setTitle(FileManager.getList()[mPos].name)
            }

            override fun onPreviousClick() {
                if (mPos != 0) {
                    mPos--
                } else
                    mPos = FileManager.getList().size - 1
                player!!.play(FileManager.getList()[mPos].absolutePath)
                player!!.setTitle(FileManager.getList()[mPos].name)
            }
        })

        player!!.play(FileManager.getList()[mPos].absolutePath)
        player!!.setTitle(FileManager.getList()[mPos].name)
    }

    override fun onPause() {
        super.onPause()
        if (player != null) {
            player!!.onPause()
        }
    }

    override fun onResume() {
        super.onResume()
        if (player != null) {
            player!!.onResume()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (player != null) {
            player!!.onDestroy()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (player != null) {
            player!!.onConfigurationChanged(newConfig)
        }
    }

    override fun onBackPressed() {
        if (player != null && player!!.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        if (DEBUG) {
            println("result $result")
        }
        return result
    }
}
