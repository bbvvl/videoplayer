package vvl.videoplayer.filesystem

import android.graphics.Bitmap
import android.util.SparseArray
import kotlinx.coroutines.experimental.async
import vvl.videoplayer.DEBUG
import vvl.videoplayer.EXTERRNAL_STORAGE_FILE
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


object FileManager {

    private val mFormat: ArrayList<String> = arrayListOf("avi", "mp4", "mkv", "webm", "3gp", "flv")
    private var mFileList: ArrayList<File> = ArrayList()
    private var mFileListBackup: ArrayList<File> = ArrayList()

    private var mFileInfo: SparseArray<String> = SparseArray()
    private var mFileBitmap: SparseArray<Bitmap> = SparseArray()

    fun setBackup(list: ArrayList<File>) {
        mFileListBackup = list
    }

    fun getBackup() = mFileListBackup

    fun getList() = mFileList

    fun setList(list: ArrayList<File>) {
        mFileList = list
    }

    fun getInfoList() = mFileInfo

    fun getBitmapList() = mFileBitmap

    private val alph = Comparator<File> { arg0, arg1 -> arg0.name.toLowerCase().compareTo(arg1.name.toLowerCase()) }
    private val sizeInc = Comparator<File> { arg0, arg1 ->
        val first = arg0.length()
        val second = arg1.length()

        println("f=  $first s= $second")
        first.compareTo(second)
    }

    private val sizeDec = Comparator<File> { arg0, arg1 ->
        val first = arg0.length()
        val second = arg1.length()

        println("f=  $first s= $second")
        second.compareTo(first)
    }

    private val type = Comparator<File> { arg0, arg1 ->

        val ext: String
        val ext2: String
        val ret: Int

        try {
            ext = arg0.name.substring(arg0.name.lastIndexOf(".") + 1, arg0.name.length).toLowerCase()
            ext2 = arg1.name.substring(arg1.name.lastIndexOf(".") + 1, arg1.name.length).toLowerCase()

        } catch (e: IndexOutOfBoundsException) {
            return@Comparator 0
        }

        ret = ext.compareTo(ext2)

        if (ret == 0) arg0.name.toLowerCase().compareTo(arg1.name.toLowerCase()) else ret
    }

    fun renameTarget(pos: Int, newName: String) = async {

        if (newName.isEmpty())
            return@async -1

        val src = mFileList[pos]
        val dest: File

        dest = File(src.parent + "/" + newName + FileUtil.getFileNameExtension(pos))

        val p = src.hashCode()
        if (src.renameTo(dest)) {
            mFileList[pos] = dest
            mFileInfo.append(dest.hashCode(), mFileInfo[p])
            mFileInfo.delete(p)
            mFileBitmap.append(dest.hashCode(), mFileBitmap[p])
            mFileBitmap.delete(p)

            return@async 0
        } else
            return@async -1
    }

    fun deleteTarget(pos: Int) = async {
        val target = mFileList[pos]

        if (DEBUG) println(" target.exists() ${target.exists()} && target.isFile ${target.isFile} && target.canWrite() ${target.canWrite()}")

        if (target.exists() && target.isFile && target.canWrite()) {
            target.delete()
            return@async 0
        }
        -1
    }

    private fun getFilesWithVideo(dir: File): ArrayList<File> {

        val listFile = dir.listFiles()
        if (listFile != null && listFile.isNotEmpty()) {
            for (i in listFile.indices) {

                if (listFile[i].isDirectory) {
                    getFilesWithVideo(listFile[i])
                } else {
                    /*if (mShowHiddenFiles)
                        mFormat.filter { listFile[i].name.endsWith(it) }.forEach {
                            mFileList.add(listFile[i])
                        }
                    else
                        if (listFile[i].absolutePath.indexOf("/.") == -1)
                            mFormat.filter { listFile[i].name.endsWith(it) }.forEach {
                                mFileList.add(listFile[i])
                            }*/

                    mFormat.filter { listFile[i].name.endsWith(it) }.forEach {
                        mFileList.add(listFile[i])
                    }

                }
            }
        }
        return mFileList
    }

    fun sort(item: Int) {
        if (item == 0) return
        val tt = mFileList.toTypedArray()
        mFileList.clear()

        when (item) {
            1 -> {
                Arrays.sort(tt, alph)
            }
            2 -> {
                Arrays.sort(tt, type)
            }
            3 -> {
                Arrays.sort(tt, sizeDec)
            }
            4 -> {
                Arrays.sort(tt, sizeInc)
            }
        }
        tt.mapTo(mFileList) { it }

    }

    fun update() = async {
        mFileList.clear()
        getFilesWithVideo(EXTERRNAL_STORAGE_FILE)
    }
}