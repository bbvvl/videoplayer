package vvl.videoplayer.filesystem

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.provider.MediaStore
import android.text.format.DateFormat
import kotlinx.coroutines.experimental.async
import vvl.videoplayer.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


object FileUtil {

    private fun getFileLength(length: Long): String {

        var l = length
        if (l > 1000) l /= 1000 else return String.format("%.1f", length.toDouble()) + "b"
        if (l > 1000) l /= 1000 else return String.format("%.1f", length.toDouble() / 1000) + "kb"
        return if (l > 1000) {
            l /= 1000;String.format("%.1f", length.toDouble() / 1000_000_000) + "gb"
        } else String.format("%.1f", length.toDouble() / 1000_000) + "mb"
    }

    private fun getFileTime(timeS: String): String {

        val time = timeS.toLong()

        val q = if (time / 1000 / 60 < 60)
            SimpleDateFormat("mm:ss")
        else
            SimpleDateFormat("HH:mm:ss")
        q.timeZone = TimeZone.getTimeZone("UTC")
        return q.format(time)
    }

    fun getFileLastModified(mod: Long, c: Context) = async {
        DateFormat.getMediumDateFormat(c).format(mod).toString()
    }

    fun getFileInfo(file: File) = async {

        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(file.absolutePath)
        val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        val width = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH)
        val height = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT)
        retriever.release()

        val a = FileUtil.getFileLength(file.length())
        val b = FileUtil.getFileTime(time)
        val c = width + "x" + height
        return@async if (width == null || height == null) "$a\n$b"
        else "$a\n$b\n$c"
    }

    fun getBitmap(c: Context, file: File) = async {
        return@async ThumbnailUtils.createVideoThumbnail(file.absolutePath, MediaStore.Images.Thumbnails.MICRO_KIND) ?: Bitmap.createScaledBitmap(BitmapFactory.decodeResource(c.resources, R.drawable.noimage), 96, 96, false)
    }

    fun getFileNameWithOutExtension(position: Int) = FileManager.getList()[position].name.substring(0, FileManager.getList()[position].name.lastIndexOf("."))

    fun getFileNameExtension(position: Int) = FileManager.getList()[position].name.substring(FileManager.getList()[position].name.lastIndexOf("."), FileManager.getList()[position].name.length)

}