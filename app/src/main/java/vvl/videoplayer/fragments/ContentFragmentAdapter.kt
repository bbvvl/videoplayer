package vvl.videoplayer.fragments

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter


class ContentFragmentAdapter(fm: FragmentManager, private var fragments: ArrayList<Fragment>) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = fragments.size

    class Holder(private val manager: FragmentManager) {
        private val fragments: ArrayList<Fragment> = ArrayList()

        fun add(f: Fragment): Holder {
            fragments.add(f)
            return this
        }

        fun set(): ContentFragmentAdapter {
            return ContentFragmentAdapter(manager, fragments)
        }
    }
}