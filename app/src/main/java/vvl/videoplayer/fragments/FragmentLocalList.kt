package vvl.videoplayer.fragments


import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.View.OVER_SCROLL_NEVER
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_list.view.*
import kotlinx.android.synthetic.main.fragment_list_item_rename.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import vvl.videoplayer.DEBUG
import vvl.videoplayer.FILEPROVIDER
import vvl.videoplayer.R
import vvl.videoplayer.activity.MainActivity
import vvl.videoplayer.activity.VideoPlayerActivity
import vvl.videoplayer.filesystem.FileManager
import vvl.videoplayer.filesystem.FileUtil
import java.io.File


class FragmentLocalList : Fragment() {


    private lateinit var mRefreshLayout: SwipeRefreshLayout
    private lateinit var mRecyclerView: RecyclerView

    private var mIsChecked = false
    private var mCheckedItem = 0

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_list, container, false)
        if (savedInstanceState != null) {
            mIsChecked = savedInstanceState.getBoolean("ischecked", false)

            mCheckedItem = savedInstanceState.getInt("ischeckeditem")
        }
        setHasOptionsMenu(true)

        initToolbar(view)
        mRefreshLayout = view.fragment_list_swipe_refresh_layout
        mRefreshLayout.setColorSchemeColors(Color.GREEN)

        mRefreshLayout.setProgressBackgroundColorSchemeColor(Color.TRANSPARENT)

        mRecyclerView = view.findViewById<RecyclerView>(R.id.fragment_list_recycler_view)
        mRecyclerView.layoutManager = LinearLayoutManager(activity)
        mRecyclerView.adapter = MyAdapter(
                object : RecyclerViewClickListener {
                    override fun onClick(position: Int) {
                        initAndStartVideoPlayer(position)
                    }
                },
                object : RecyclerViewLongClickListener {
                    override fun onLongClick(position: Int) {

                        val adb = AlertDialog.Builder(this@FragmentLocalList.context)
                        adb.setTitle(FileManager.getList()[position].name)

                        val s = arrayOf("Play", "Rename", "Delete", "Share")
                        adb.setItems(s) { dialog, which ->

                            println("which= $which")
                            dialog.dismiss()

                            when (which) {
                                0 -> initAndStartVideoPlayer(position)
                                1 -> {
                                    val adb1 = AlertDialog.Builder(this@FragmentLocalList.context)

                                    val view1 = activity.layoutInflater.inflate(R.layout.fragment_list_item_rename, null)

                                    val edit = view1.findViewById<EditText>(R.id.fragment_list_item_rename_edit_text)
                                    edit.setText(FileUtil.getFileNameWithOutExtension(position))
                                    edit.setSelection(0, edit.text.length)
                                    adb1.setView(view1)
                                    adb1.setTitle("Rename")
                                    adb1.setMessage("${FileManager.getList()[position].name} -> [?]")
                                    val ad1 = adb1.create()

                                    edit.addTextChangedListener(object : TextWatcher {

                                        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                                        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                                            view1.fragment_list_item_rename_button_ok.isEnabled = !s!!.isEmpty()
                                        }

                                        override fun afterTextChanged(s: Editable?) {}
                                    })
                                    view1.fragment_list_item_rename_button_cancel.setOnClickListener { ad1.dismiss() }
                                    view1.fragment_list_item_rename_button_ok.setOnClickListener {
                                        this@FragmentLocalList.view!!.fragment_list_swipe_refresh_layout.isRefreshing = true;
                                        ad1.dismiss()
                                        async(UI) {
                                            val text = if (FileManager.renameTarget(position, edit.text.toString()).await() == 0) {
                                                mRecyclerView.adapter.notifyItemChanged(position)
                                                "Rename was successful"
                                            } else "Rename was unsuccessful"

                                            this@FragmentLocalList.view!!.fragment_list_swipe_refresh_layout.isRefreshing = false
                                            Toast.makeText(this@FragmentLocalList.context, text, Toast.LENGTH_SHORT).show()

                                        }
                                    }
                                    ad1.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)



                                    ad1.show()
                                }
                                2 -> {

                                    val adb2 = AlertDialog.Builder(this@FragmentLocalList.context)

                                    adb2.setTitle("Delete")
                                    adb2.setMessage(FileManager.getList()[position].name)
                                    adb2.setNegativeButton(android.R.string.cancel, null)
                                    adb2.setPositiveButton(android.R.string.ok) { dialog1, _ ->
                                        dialog1.dismiss()
                                        view.fragment_list_swipe_refresh_layout.isRefreshing = true
                                        launch(UI) {
                                            val pos = FileManager.getList()[position].hashCode()
                                            val text = if (FileManager.deleteTarget(position).await() == 0) {

                                                FileManager.getInfoList().delete(pos)
                                                FileManager.getBitmapList().delete(pos)
                                                FileManager.update().await()
                                                mRecyclerView.adapter.notifyItemRemoved(position)
                                                "Delete was successful"
                                            } else "Delete was unsuccessful"
                                            view.fragment_list_swipe_refresh_layout.isRefreshing = false
                                            Toast.makeText(this@FragmentLocalList.context, text, Toast.LENGTH_SHORT).show()
                                        }

                                    }

                                    adb2.create().show()
                                }
                                3 -> {
                                    val photoURI: Uri = FileProvider.getUriForFile(activity,
                                            FILEPROVIDER,
                                            (FileManager.getList()[position]))


                                    val shareIntent = Intent()
                                    shareIntent.setAction(Intent.ACTION_SEND);
                                    shareIntent.putExtra(Intent.EXTRA_STREAM, photoURI)
                                    shareIntent.type = "video/*";
                                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    startActivity(Intent.createChooser(shareIntent, "Send"));
                                }
                            }
                        }
                        adb.setNegativeButton(android.R.string.cancel, null)
                        adb.create().show()
                    }


                }, activity

        )

        mRefreshLayout.setOnRefreshListener {
            if (DEBUG) println("setOnRefreshListener")

            (mRecyclerView.adapter as MyAdapter).isClickable = false
            mRefreshLayout.isRefreshing = true
            async(UI) {
                val hc = FileManager.getList().hashCode()
                if (DEBUG) println("$hc")
                FileManager.update().await()
                update()
                if (hc != FileManager.getList().hashCode()) {

                    FileManager.getInfoList().clear()
                    FileManager.getBitmapList().clear()
                    for (q in FileManager.getList()) {
                        FileManager.getInfoList().append(q.hashCode(), FileUtil.getFileInfo(q).await())
                        FileManager.getBitmapList().append(q.hashCode(), FileUtil.getBitmap(activity, q).await())
                    }

                    mRecyclerView.adapter.notifyDataSetChanged()
                }
                mRefreshLayout.isRefreshing = false
                (mRecyclerView.adapter as MyAdapter).isClickable = true
            }
        }
        mRecyclerView.overScrollMode = OVER_SCROLL_NEVER
        update()
        return view
    }

    private fun initToolbar(view: View) {
        val mToolbar = view.fragment_list_toolbar
        (activity as MainActivity).setSupportActionBar(mToolbar)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putBoolean("ischecked", mIsChecked)
        outState.putInt("ischeckeditem", mCheckedItem)
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (DEBUG) println("onCreateOptionsMenu")
        inflater!!.inflate(R.menu.fragment_list_menu, menu)

        menu!!.findItem(R.id.fragment_list_menu_check_box).isChecked = mIsChecked
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (DEBUG) println("onOptionsItemSelected")

        if (mRefreshLayout.isRefreshing) return super.onOptionsItemSelected(item)

        return when (item!!.itemId) {
            R.id.fragment_list_menu_check_box -> {
                item.isChecked = !item.isChecked
                mIsChecked = item.isChecked
                update()
                true
            }
            R.id.fragment_list_menu_sort -> {

                val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
                builder.setTitle("Sort by")
                val s = arrayOf("None", "Name", "Type", "Size ↑", "Size ↓")
                builder.setSingleChoiceItems(s, mCheckedItem) { dialog, which ->

                    dialog.dismiss()
                    mCheckedItem = which
                    update()
                }
                builder.create().show()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }


    }


    private fun initAndStartVideoPlayer(position: Int) {
        val i = Intent(activity, VideoPlayerActivity::class.java)
        i.putExtra("url", position)
        startActivity(i)
    }

    private fun update() {

        mRefreshLayout.isRefreshing = true
        if (mIsChecked) {
            if (FileManager.getBackup().size != 0) FileManager.setList(FileManager.getBackup().clone() as ArrayList<File>)
            else Toast.makeText(activity, "OUPS...", Toast.LENGTH_SHORT).show()
        } else {
            FileManager.getList().clear()
            if (DEBUG) println(FileManager.getBackup().size)
            FileManager.getBackup()
                    .filter {
                        it.absolutePath.indexOf("/.") == -1
                    }
                    .forEach { FileManager.getList().add(it) }

        }

        FileManager.sort(mCheckedItem)

        mRefreshLayout.isRefreshing = false
        mRecyclerView.adapter.notifyDataSetChanged()
    }

    companion object {

        fun newInstance(): Fragment {
            val args = Bundle()
            //args.putInt("position", position)
            val fragment = FragmentLocalList()
            fragment.arguments = args
            return fragment
        }
    }
}

