package vvl.videoplayer.fragments

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import vvl.videoplayer.R
import vvl.videoplayer.filesystem.FileManager
import vvl.videoplayer.filesystem.FileUtil


class MyAdapter(listener: RecyclerViewClickListener, longListener: RecyclerViewLongClickListener, context: Context) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {


    private val mListener: RecyclerViewClickListener? = listener
    private val mLongListener: RecyclerViewLongClickListener? = longListener
    private val mContext: Context = context
    var isClickable: Boolean = true


    inner class ViewHolder(v: View, listener: RecyclerViewClickListener, longListener: RecyclerViewLongClickListener) : RecyclerView.ViewHolder(v), View.OnClickListener, View.OnLongClickListener {
        init {
            v.setOnClickListener(this)
            v.setOnLongClickListener(this)
        }

        private val mListener: RecyclerViewClickListener? = listener
        private var mLongListener: RecyclerViewLongClickListener? = longListener

        var mTextViewName: TextView = v.findViewById<TextView>(R.id.fragment_list_item_text_view_name)
        var mTextViewDate: TextView = v.findViewById<TextView>(R.id.fragment_list_item_text_view_date)
        var mTextViewInfo: TextView = v.findViewById<TextView>(R.id.fragment_list_item_text_view_info)
        var mImageView: ImageView = v.findViewById<ImageView>(R.id.fragment_list_item_image_view)
        override fun onClick(view: View) {
            if (isClickable) mListener!!.onClick(adapterPosition)
        }

        override fun onLongClick(v: View): Boolean {
            if (isClickable) mLongListener!!.onLongClick(adapterPosition)
            return true
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder? {

        val v = LayoutInflater.from(parent!!.context)
                .inflate(R.layout.fragment_list_item, parent, false)
        return ViewHolder(v, mListener!!, mLongListener!!)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        async(UI) {
            val file = FileManager.getList()[position]
            holder.mTextViewName.text = file.name
            holder.mTextViewDate.text = FileUtil.getFileLastModified(file.lastModified(), mContext).await()
            val pos = file.hashCode()
            holder.mTextViewInfo.text = FileManager.getInfoList()[pos] //FileUtil.getFileInfo(file).await()
            holder.mImageView.setImageBitmap(FileManager.getBitmapList()[pos])//FileUtil.getBitmap(file).await())
        }
    }

    override fun getItemCount(): Int = FileManager.getList().size

}