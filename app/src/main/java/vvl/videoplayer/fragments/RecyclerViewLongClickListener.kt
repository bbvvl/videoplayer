package vvl.videoplayer.fragments


interface RecyclerViewLongClickListener {

    fun onLongClick(position: Int)

}