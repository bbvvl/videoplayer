package vvl.videoplayer.viewpager

import android.support.v4.view.ViewPager
import android.view.View

class ZoomOutTransformer : ViewPager.PageTransformer {
    private val sMinScale = 0.0f

    override fun transformPage(view: View, position: Float) {
        val alpha: Float
        if (position in -1.0..0.0) {
            val scaleFactor = Math.max(sMinScale, 1 - Math.abs(position))
            view.scaleX = scaleFactor
            view.scaleY = scaleFactor
            alpha = position + 1
        } else alpha = 1 - position

        view.alpha = alpha

    }

}
